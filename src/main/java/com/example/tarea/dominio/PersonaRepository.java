package com.example.tarea.dominio;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Luis on 21/07/2017.
 */
public interface PersonaRepository extends JpaRepository<Persona, Long>{
}
