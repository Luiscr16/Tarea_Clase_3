package com.example.tarea.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by Luis on 21/07/2017.
 */
@Entity
public class Persona {
    private String Nombre;
    private String Direccion;
    private int edad;

    @Id@GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    public long getId() {return id;}

    public void setId(long id) {this.id =id;}

    public Persona() {

    }

    public Persona(String nombre, String direccion, int edad) {
        Nombre = nombre;
        Direccion = direccion;
        this.edad = edad;
    }

    public String getNombre() {return Nombre;}
    public int getEdad() {return edad;}
    public void setNombre (String nombre){Nombre = nombre;}
    public void setEdad(int edad) {this.edad = edad;}

    public String getDireccion() {return Direccion;}
    public void setDireccion(String direccion) {Direccion=direccion;}
}
