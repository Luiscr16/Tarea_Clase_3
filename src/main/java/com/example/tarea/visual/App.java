package com.example.tarea.visual;

import com.example.tarea.dominio.Persona;
import com.example.tarea.dominio.PersonaRepository;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Luis on 19/07/2017.
 */
@SpringUI
public class App extends UI{
    @Autowired
    PersonaRepository repo;
@Override
    protected void init(VaadinRequest vaadinRequest){
        VerticalLayout layout=new VerticalLayout();
        HorizontalLayout hlayout=new HorizontalLayout();
        HorizontalLayout hlayout2=new HorizontalLayout();
        HorizontalLayout hlayout3=new HorizontalLayout();
        Label titulo=new Label("<h1> BIENVENIDO AL SISTEMA </h1>", ContentMode.HTML);
        Button alumno=new Button("Interfaz de Alumnos");
        Button curso=new Button("Interfaz de Cursos");
        Button grado=new Button("Interfaz de Grado");
        TextField nombre=new TextField("Nombre");
        TextField direccion=new TextField("Dirección");
        TextField edad=new TextField("Edad");
        Button agregar=new Button("Agregar");
        Button eliminar=new Button("Eliminar");
        hlayout.addComponents(alumno,curso,grado);
        hlayout2.addComponents(nombre,direccion,edad);
        hlayout3.addComponents(agregar, eliminar);

        Grid<Persona> persona = new Grid<>();
        persona.addColumn(Persona::getId).setCaption("ID");
        persona.addColumn(Persona::getNombre).setCaption("Nombre");
        persona.addColumn(Persona::getDireccion).setCaption("Dirección");
        persona.addColumn(Persona::getEdad).setCaption("Edad");

        agregar.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Persona estudiante= new Persona();
                estudiante.setNombre(nombre.getValue());
                estudiante.setDireccion(direccion.getValue());
                estudiante.setEdad(Integer.parseInt(edad.getValue()));
                repo.save(estudiante);

                persona.setItems(repo.findAll());
                nombre.clear();
                direccion.clear();
                edad.clear();

                Notification.show("Agregado con Exito");
            }
        });
        layout.addComponents(titulo,hlayout,hlayout2,persona,hlayout3);
        layout.setComponentAlignment(titulo,Alignment.BOTTOM_CENTER);
        setContent(layout);

    }
}
